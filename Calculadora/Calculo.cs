﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculadora
{
    class Calculo
    {
        public double Operador1 { get; set; }
        public double Operador2 { get; set; }

        public string Simbolo { get; set; }

        public double Calcular()
        {
            double resultado;

            resultado = 0;

            switch (this.Simbolo)
            {
                case "+":
                    resultado = this.Operador1 + this.Operador2;
                    break;
                case "-":
                    resultado = this.Operador1 - this.Operador2;
                    break;
                case "*":
                    resultado = this.Operador1 * this.Operador2;
                    break;
                case "/":
                    resultado = this.Operador1 / this.Operador2;
                    break;
            }

            return resultado;
        }

        public void Reiniciar()
        {
            this.Operador1 = 0;
            this.Operador2 = 0;
            this.Simbolo = string.Empty;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculadora
{
    public partial class Form1 : Form
    {
        private bool prepararOperacion;
        private Calculo calc;
        private const int MAXDIGITOS = 5;
        private int contadorKeyPress;
        private int contadorKeyDown;

        public Form1()
        {
            InitializeComponent();

            this.prepararOperacion = false;

            calc = new Calculo();

            EstablecerEventosKey();
        }

        #region Metodos propios
        // Esta funcion vincula todos los controles que estan directamente dentro de la forma
        // con los metodos KeyPress y KeyDown
        private void EstablecerEventosKey()
        {
            foreach(Control control in this.Controls)
            {
                if (control is Button)
                {
                    control.KeyPress += new KeyPressEventHandler(this.Form1_KeyPress);
                    control.KeyDown += new KeyEventHandler(this.Form1_KeyDown);
                    control.KeyUp += new KeyEventHandler(this.Form1_KeyUp);
                }
            }
        }

        // Si un caracter a la vez. Si solo queda un digito se coloca cero. Si fuese cero
        // ya no se sigue ejecutando.
        private void EliminarCaracterPantalla()
        {
            string texto;
            int longitudInicial;

            texto = lblPantalla.Text;
            longitudInicial = texto.Length;

            // Al comparar si dos objetos son iguales lo mas indicado es utilizar la funcion 'equals'
            if (texto.Equals("0"))
            {
                return;
            }
            else if (longitudInicial == 1)
            {
                lblPantalla.Text = "0";
            }
            else
            {
                lblPantalla.Text = texto.Remove(longitudInicial - 1, 1);
            }

        }

        private double ConvertirTextoADouble()
        {
            double numero;
            bool seParseo;

            seParseo = Double.TryParse(lblPantalla.Text, out numero);

            if (seParseo)
            {
                return numero;
            }
            else
            {
                return 0;
            }
        }
        #endregion

        private void ButtonNumerico_Click(object sender, EventArgs e)
        {
            Button boton = sender as Button;

            double numero;
            bool existePunto;
            bool seConcatena;

            numero = ConvertirTextoADouble();
            existePunto = lblPantalla.Text.IndexOf(".") >= 0;
            seConcatena = false;

            if (existePunto)
            {
                seConcatena = true;
            }
            else if (numero != 0.0)
            {
                seConcatena = true;
            }

            if (seConcatena && prepararOperacion)
            {
                seConcatena = false;
                prepararOperacion = false;
            }

            if (seConcatena)
            {
                lblPantalla.Text += boton.Text;
            }
            else
            {
                lblPantalla.Text = boton.Text;
            }
        }

        private void ButtonPunto_Click(object sender, EventArgs e)
        {
            if (lblPantalla.Text.IndexOf(".") < 0)
            {
                lblPantalla.Text += ".";
            }
        }

        private void ButtonOperativos_Click(object sender, EventArgs e)
        {
            Button boton;

            this.prepararOperacion = true;
            calc.Operador1 = ConvertirTextoADouble();

            boton = sender as Button;
            calc.Simbolo = boton.Text;
        }

        private void ButtonIgual_Click(object sender, EventArgs e)
        {
            double resultado;

            calc.Operador2 = ConvertirTextoADouble();
            resultado = calc.Calcular();

            lblPantalla.Text = resultado.ToString();
        }

        // Esta funcion permite que al presionar una tecla se defina cual es
        // el metodo que debe utilizarse. Para que funcione correctamente es
        // necesario que el evento esta aplicado en cada componente que pueda
        // tener foco.
        private void Form1_KeyPress(object sender, KeyPressEventArgs e)
        {
            switch(e.KeyChar)
            {
                case '0':
                    ButtonNumerico_Click(btn0, e);
                    break;
                case '1':
                    ButtonNumerico_Click(btn1, e);
                    break;
                case '2':
                    ButtonNumerico_Click(btn2, e);
                    break;
                case '3':
                    ButtonNumerico_Click(btn3, e);
                    break;
                case '4':
                    ButtonNumerico_Click(btn4, e);
                    break;
                case '5':
                    ButtonNumerico_Click(btn5, e);
                    break;
                case '6':
                    ButtonNumerico_Click(btn6, e);
                    break;
                case '7':
                    ButtonNumerico_Click(btn7, e);
                    break;
                case '8':
                    ButtonNumerico_Click(btn8, e);
                    break;
                case '9':
                    ButtonNumerico_Click(btn9, e);
                    break;
                case '.':
                    // En este caso no es necesario saber que objeto genera
                    // este evento por lo que puede referenciarse null
                    ButtonPunto_Click(null, e);
                    break;
                case '+':
                    ButtonOperativos_Click(btnSuma, e);
                    break;
                case '-':
                    ButtonOperativos_Click(btnResta, e);
                    break;
                case '*':
                    ButtonOperativos_Click(btnMult, e);
                    break;
                case '/':
                    ButtonOperativos_Click(btnDiv, e);
                    break;
                case '=':
                    ButtonIgual_Click(null, e);
                    break;
                // En este caso al presionar C (mayuscula o minuscula) genera
                // el evento para limpiar la pantalla
                case 'c':
                    btnC_Click(null, null);
                    break;
            }

            listBox1.Items.Add("");
            listBox1.Items.Add($"Evento KeyPress: {this.contadorKeyPress++}");
            listBox1.Items.Add($"\tTecla presionada: {e.KeyChar}");
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Back)
            {
                EliminarCaracterPantalla();
            } else if (e.KeyCode == Keys.Delete)
            {
                btnCE_Click(sender, e);
            }

            // Agregar elementos al listbox
            listBox1.Items.Clear();
            listBox1.Items.Add($"Evento KeyDown: {this.contadorKeyDown++}");
            listBox1.Items.Add($"\tAlt: {(e.Alt ? "Si" : "No")}");
            listBox1.Items.Add($"\tShift: {(e.Shift ? "Si" : "No")}");
            listBox1.Items.Add($"\tCtrl: {(e.Control ? "Si" : "No")}");
            listBox1.Items.Add($"\tKeyCode: {e.KeyCode}");
            listBox1.Items.Add($"\tKeyData: {e.KeyData}");
            listBox1.Items.Add($"\tKeyValue: {e.KeyValue}");
        }

        private void btnC_Click(object sender, EventArgs e)
        {
            btnCE_Click(sender, e);
            this.prepararOperacion = false;
            calc.Reiniciar();
        }

        private void btnCE_Click(object sender, EventArgs e)
        {
            lblPantalla.Text = "0";
        }

        private void Form1_KeyUp(object sender, KeyEventArgs e)
        {
            listBox1.Items.Add("");
            listBox1.Items.Add("Evento KeyUp");

            this.contadorKeyDown = 0;
            this.contadorKeyPress = 0;
        }
    }
}
